// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2021
 * Author: Ricardo Martincoski <ricardo.martincoski@gmail.com>
 */

#include <kunit/test.h>

static struct mock_struct {
	struct usb_control_msg_data {
		int result;
		u32 ocp_data;
	} usb_control_msg;
} mock;

static int Register_netdev(void *dev)
{
	return 0;
}

static int Sysfs_create_group(void *kobj, void *grp)
{
	return 0;
}

static int Usb_control_msg(void *dev, unsigned int pipe,
	__u8 request, __u8 requesttype, __u16 value, __u16 index,
	void *data, __u16 size, int timeout)
{
	*((u32 *) data) = cpu_to_le32(mock.usb_control_msg.ocp_data << 16);
	return mock.usb_control_msg.result;
}

#define CONFIG_USB_RTL8152_TEST_OVERRIDE
#include "r8152.c" /* allow access to static functions */

static int r8152_common_init(struct kunit *test)
{
#define CREATE_MOCK(type) \
	struct type *mock_##type; \
	mock_##type = kunit_kzalloc(test, sizeof(struct type), GFP_KERNEL); \
	KUNIT_ASSERT_NOT_ERR_OR_NULL(test, mock_##type)

	kunit_info(test, "initializing mocked objects\n");

	CREATE_MOCK(usb_device);
	CREATE_MOCK(usb_host_config);
	CREATE_MOCK(usb_host_interface);
	CREATE_MOCK(usb_interface);

	mock_usb_interface->cur_altsetting = mock_usb_host_interface;
	mock_usb_interface->dev.parent = &mock_usb_device->dev;
	mock_usb_device->actconfig = mock_usb_host_config;
	mock_usb_host_interface->desc.bInterfaceClass = USB_CLASS_VENDOR_SPEC;
	mock_usb_host_interface->desc.bNumEndpoints = 3;

	test->priv = mock_usb_interface;

	kunit_info(test, "initializing mocked values\n");
	mock.usb_control_msg.result = 1;
	mock.usb_control_msg.ocp_data = 0x4c00;
	return 0;
}

static void r8152_getversion_valid_version(struct kunit *test)
{
	struct usb_interface *intf = test->priv;

	mock.usb_control_msg.ocp_data = 0x4c00;
	KUNIT_EXPECT_EQ(test, rtl_get_version(intf), (u8) RTL_VER_01);

	mock.usb_control_msg.ocp_data = 0x4c10;
	KUNIT_EXPECT_EQ(test, rtl_get_version(intf), (u8) RTL_VER_02);

	mock.usb_control_msg.ocp_data = 0x5c00;
	KUNIT_EXPECT_EQ(test, rtl_get_version(intf), (u8) RTL_VER_03);
}

static void r8152_getversion_invalid_version(struct kunit *test)
{
	struct usb_interface *intf = test->priv;

	mock.usb_control_msg.ocp_data = 0x1110;
	KUNIT_EXPECT_EQ(test, rtl_get_version(intf), (u8) RTL_VER_UNKNOWN);
}

static void r8152_getversion_invalid_device(struct kunit *test)
{
	struct usb_interface *intf = test->priv;

	mock.usb_control_msg.result = -ENODEV;
	KUNIT_EXPECT_EQ(test, rtl_get_version(intf), (u8) 0);
}

static void r8152_probe_invalid_version(struct kunit *test)
{
	struct usb_interface *intf = test->priv;

	mock.usb_control_msg.ocp_data = 0x1110;
	KUNIT_EXPECT_EQ(test, rtl8152_probe(intf, NULL), -ENODEV);
}

static void r8152_probe_fills_device_info(struct kunit *test)
{
	struct usb_interface *intf = test->priv;

	struct r8152 *tp;

	tp = usb_get_intfdata(intf);
	KUNIT_ASSERT_EQ(test, tp, NULL);

	KUNIT_EXPECT_EQ(test, rtl8152_probe(intf, NULL), 0);

	tp = usb_get_intfdata(intf);
	KUNIT_ASSERT_NOT_ERR_OR_NULL(test, tp);
	KUNIT_EXPECT_NOT_ERR_OR_NULL(test, tp->udev);
	KUNIT_EXPECT_NOT_ERR_OR_NULL(test, tp->intf);
	KUNIT_EXPECT_NOT_ERR_OR_NULL(test, tp->netdev);
}

static void r8152_probe_fills_driver_ops(struct kunit *test)
{
	struct usb_interface *intf = test->priv;

	struct r8152 *tp;
	struct rtl_ops *ops;

	tp = usb_get_intfdata(intf);
	KUNIT_ASSERT_EQ(test, tp, NULL);

	KUNIT_EXPECT_EQ(test, rtl8152_probe(intf, NULL), 0);

	tp = usb_get_intfdata(intf);
	KUNIT_ASSERT_NOT_ERR_OR_NULL(test, tp);

	ops = &tp->rtl_ops;
	KUNIT_ASSERT_NOT_ERR_OR_NULL(test, ops);
	KUNIT_EXPECT_NOT_ERR_OR_NULL(test, ops->init);
	KUNIT_EXPECT_NOT_ERR_OR_NULL(test, ops->enable);
	KUNIT_EXPECT_NOT_ERR_OR_NULL(test, ops->disable);
	KUNIT_EXPECT_NOT_ERR_OR_NULL(test, ops->up);
	KUNIT_EXPECT_NOT_ERR_OR_NULL(test, ops->down);
}

static void r8152_probe_fills_driver_ops_depending_on_version(struct kunit *test)
{
	struct usb_interface *intf = test->priv;

	struct r8152 *tp;
	struct rtl_ops *ops;

	mock.usb_control_msg.ocp_data = 0x4c00;
	KUNIT_EXPECT_EQ(test, rtl8152_probe(intf, NULL), 0);
	tp = usb_get_intfdata(intf);
	KUNIT_ASSERT_NOT_ERR_OR_NULL(test, tp);
	ops = &tp->rtl_ops;
	KUNIT_ASSERT_NOT_ERR_OR_NULL(test, ops);
	KUNIT_EXPECT_PTR_EQ(test, ops->init, &r8152b_init);

	mock.usb_control_msg.ocp_data = 0x5c00;
	KUNIT_EXPECT_EQ(test, rtl8152_probe(intf, NULL), 0);
	tp = usb_get_intfdata(intf);
	KUNIT_ASSERT_NOT_ERR_OR_NULL(test, tp);
	ops = &tp->rtl_ops;
	KUNIT_ASSERT_NOT_ERR_OR_NULL(test, ops);
	KUNIT_EXPECT_PTR_EQ(test, ops->init, &r8153_init);
}

static struct kunit_case r8152_getversion_testcases[] = {
	KUNIT_CASE(r8152_getversion_valid_version),
	KUNIT_CASE(r8152_getversion_invalid_version),
	KUNIT_CASE(r8152_getversion_invalid_device),
	{}
};

static struct kunit_case r8152_probe_testcases[] = {
	KUNIT_CASE(r8152_probe_invalid_version),
	KUNIT_CASE(r8152_probe_fills_device_info),
	KUNIT_CASE(r8152_probe_fills_driver_ops),
	KUNIT_CASE(r8152_probe_fills_driver_ops_depending_on_version),
	{}
};

static struct kunit_suite r8152_getversion_testsuite = {
	.name = "r8152-getversion-test",
	.init = r8152_common_init,
	.test_cases = r8152_getversion_testcases,
};

static struct kunit_suite r8152_probe_testsuite = {
	.name = "r8152-probe-test",
	.init = r8152_common_init,
	.test_cases = r8152_probe_testcases,
};

kunit_test_suites(&r8152_getversion_testsuite, &r8152_probe_testsuite);

MODULE_DESCRIPTION("Unit tests for r8152");
MODULE_AUTHOR("Ricardo Martincoski");
MODULE_LICENSE("GPL v2");
