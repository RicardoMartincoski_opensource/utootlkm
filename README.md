[TOC]

# utootlkm

------

## Description
**utootlkm** stands for *Unit Tests for Out-Of-Tree Linux Kernel Modules*.

This project can be used as an infrastructure to run unit tests for any open
source Linux kernel module that is maintained in its own repository (out of the
main Linux git tree).

------

## License

SPDX-License-Identifier: GPL-2.0-only

    utootlkm
    Copyright (C) 2021  Ricardo Martincoski  <ricardo.martincoski@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

------

There are some files in the tree copied from other projects.
All of such files were originally licensed using a license that allows them to
be redistributed following GPL-2.0.
They have the license stated in the header of the file. For instance:

    SPDX-License-Identifier: GPL-2.0-or-later
    copied from <Project name> <version>
    and then updated:
    - to do <something nice>

------

## How to run the tests

Using a computer with `Ubuntu 20.04.2.0` and Internet access, execute following
steps that would take around 1 to 2 hours, depending on your computer and your
Internet speed, and around 6 GB of hard drive.

1) Install `docker` and add user to group:
```
$ sudo apt install docker.io
$ sudo groupadd docker
$ sudo usermod -aG docker $USER # NOTE: logout/login after this
$ docker run hello-world
```

2) Install `git` and `make`:
```
$ sudo apt install git make
```

3) Download the repo and run the tests
```
$ git clone --depth 1 --recurse-submodules -b v1.1 \
  https://gitlab.com/RicardoMartincoski_opensource/utootlkm.git
$ make -C utootlkm/
```
