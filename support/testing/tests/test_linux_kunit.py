import os
import subprocess

import infra

# expose expected output and inner results to the shell calling 'make test'
EXPOSE_INNER_RESULTS_TO_SHELL = os.getenv("V") == '1'


class TestLinuxKunitQemu(infra.basetest.BRTest):
    br2_external = [os.getenv("BR2_EXTERNAL_DIR")]

    def call_on_host(self, args, env=None, cwd=None, logfile=None):
        """Call a script and return stdout and stderr as lists."""
        if logfile:
            logfile.write(cwd + "$ " + " ".join(args) + "\n")
            logfile.flush()
        p = subprocess.Popen(args, cwd=cwd, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, env=env,
                             universal_newlines=True)
        out, err = p.communicate()
        ret = p.returncode
        if logfile:
            if out:
                logfile.write(out + "\n")
            if err:
                logfile.write("STDERR: " + err + "\n")
            if ret != 0:
                logfile.write("RETURNCODE: " + str(ret) + "\n")
            logfile.flush()
        return ret, out.splitlines(), err.splitlines()

    def emulator_run(self, cmd, timeout=-1):
        output, exit_code = self.emulator.run(cmd, timeout)
        self.assertEqual(exit_code, 0, "command '{}' failed".format(cmd))
        return output

    def emulator_run_special_prompt(self, cmd):
        def run(cmd, prompt):
            self.emulator.qemu.sendline(cmd)
            self.emulator.qemu.expect(cmd)
            self.emulator.qemu.expect(prompt)

        default_prompt = "# "
        special_prompt = "this is the prompt# "

        run('export PS1="{}"'.format(special_prompt), special_prompt)

        run(cmd, special_prompt)
        # Remove double carriage return from qemu stdout so str.splitlines()
        # works as expected.
        output = self.emulator.qemu.before.replace("\r\r", "\r").splitlines()[1:]

        run('export PS1="{}"'.format(default_prompt), default_prompt)

        return output

    def cleanup_dmesg(self):
        self.emulator_run("dmesg -c >/dev/null")

    def start_kunit_on_module(self, module, timeout=60):
        self.emulator_run("mount -t debugfs none /sys/kernel/debug")
        self.emulator_run("time modprobe {}".format(module), timeout=timeout)

    def save_kunit_output(self, filename):
        testsuites = self.emulator_run("ls -1 --color=never /sys/kernel/debug/kunit/")
        full_text = "TAP version 14\n1..{}\n".format(len(testsuites))
        for suite in testsuites:
            output = self.emulator_run_special_prompt("cat /sys/kernel/debug/kunit/{}/results".format(suite))
            full_text += '\n'.join(output) + '\n'

        with open(filename, 'w') as f:
            f.write(full_text)
            f.flush()

    def save_dmesg(self, filename):
        output = self.emulator_run_special_prompt("dmesg")
        full_text = '\n'.join(output) + '\n'

        with open(filename, 'w') as f:
            f.write(full_text)
            f.flush()

    def process_kunit_output(self, filename, linux_dir, logfile):
        _, output, _ = self.call_on_host(["./tools/testing/kunit/kunit.py", "parse", filename], cwd=linux_dir, logfile=logfile)
        # suppress warning generated because kunit result from debugfs always print 1 as suite_index
        filtered = [line for line in output if 'expected_suite_index' not in line]
        return filtered

    def check_kunit_output(self, output, run=None, failed=0, crashed=0):
        expected_result_line = 'Testing complete.'
        expected_run = " {} tests run."
        expected_failed = " {} failed.".format(failed)
        expected_crashed = " {} crashed.".format(crashed)

        if EXPOSE_INNER_RESULTS_TO_SHELL:
            self.show_msg('EXPECTING FROM KUNIT: {}{}{}{} KUNIT OUTPUT:\n{}\n'.format(
                  expected_result_line, expected_run.format(run or '*'), expected_failed, expected_crashed, '\n'.join(output)))

        result = None
        for line in output[-3:]:
            if expected_result_line in line:
                result = line
                break
        self.assertIsNotNone(result)
        self.assertIn(expected_crashed, result)
        self.assertIn(expected_failed, result)
        if run:
            self.assertIn(expected_run.format(run or 0), result)
        else:
            self.assertNotIn(expected_run.format(run or 0), result)
