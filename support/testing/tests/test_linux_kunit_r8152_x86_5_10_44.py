import os

import infra
from tests.utootlkm.test_linux_kunit import TestLinuxKunitQemu

KERNEL_VERSION = "5.10.44"


class TestLinuxKunitQemuR8152_X86_5_10_44(TestLinuxKunitQemu):
    config = \
        """
        BR2_x86_core2=y
        BR2_CCACHE=y
        BR2_CCACHE_DIR="$(BR2_EXTERNAL_UTOOTLKM_PATH)/.ccache/x86-core2--glibc--bleeding-edge-2018.11-1"
        BR2_TOOLCHAIN_EXTERNAL=y
        BR2_TOOLCHAIN_EXTERNAL_CUSTOM=y
        BR2_TOOLCHAIN_EXTERNAL_DOWNLOAD=y
        BR2_TOOLCHAIN_EXTERNAL_URL="http://toolchains.bootlin.com/downloads/releases/toolchains/x86-core2/tarballs/x86-core2--glibc--bleeding-edge-2018.11-1.tar.bz2"
        BR2_TOOLCHAIN_EXTERNAL_GCC_8=y
        BR2_TOOLCHAIN_EXTERNAL_HEADERS_4_14=y
        BR2_TOOLCHAIN_EXTERNAL_CXX=y
        BR2_TOOLCHAIN_EXTERNAL_HAS_SSP=y
        BR2_TOOLCHAIN_EXTERNAL_CUSTOM_GLIBC=y
        BR2_TARGET_GENERIC_GETTY_PORT="ttyS0"
        BR2_LINUX_KERNEL=y
        BR2_LINUX_KERNEL_CUSTOM_VERSION=y
        BR2_LINUX_KERNEL_CUSTOM_VERSION_VALUE="{}"
        BR2_LINUX_KERNEL_USE_CUSTOM_CONFIG=y
        BR2_LINUX_KERNEL_CUSTOM_CONFIG_FILE="board/qemu/x86/linux.config"
        BR2_TARGET_ROOTFS_EXT2=y
        # BR2_TARGET_ROOTFS_TAR is not set
        BR2_PACKAGE_REALTEK_R8152_LINUX=y
        BR2_PACKAGE_REALTEK_R8152_LINUX_TEST=y
        """.format(KERNEL_VERSION)

    def force_rebuild_of_driver(self):
        self.show_msg("Rebuilding driver under test")
        self.b.build(["realtek-r8152-linux-dirclean", "all"])

    def boot(self):
        img = os.path.join(self.builddir, "images", "rootfs.ext2")
        self.emulator.boot(arch="i386",
                           kernel=os.path.join(self.builddir, "images", "bzImage"),
                           kernel_cmdline=["root=/dev/vda console=ttyS0"],
                           options=["-drive", "file={},if=virtio,format=raw".format(img),
                                    "-M", "pc"])
        self.emulator.login()

    def test_run(self):
        self.force_rebuild_of_driver()
        self.boot()

        self.cleanup_dmesg()

        self.start_kunit_on_module("r8152-test", timeout=120)

        kunitresultfilename = "{}-{}.log".format(self.builddir, "kunitresult")
        self.save_kunit_output(kunitresultfilename)

        dmesgfilename = "{}-{}.log".format(self.builddir, "dmesg")
        self.save_dmesg(dmesgfilename)

        linux_dir = os.path.join(self.builddir, "build", "linux-{}".format(KERNEL_VERSION))
        logfile = infra.open_log_file(self.builddir, "run", self.logtofile)
        output = self.process_kunit_output(kunitresultfilename, linux_dir, logfile)
        self.check_kunit_output(output)
