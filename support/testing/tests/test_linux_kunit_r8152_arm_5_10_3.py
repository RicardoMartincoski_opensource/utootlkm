import os

import infra
from tests.utootlkm.test_linux_kunit import TestLinuxKunitQemu

KERNEL_VERSION = "5.10.3"


class TestLinuxKunitQemuR8152_Arm_5_10_3(TestLinuxKunitQemu):
    config = \
        """
        BR2_arm=y
        BR2_CCACHE=y
        BR2_CCACHE_DIR="$(BR2_EXTERNAL_UTOOTLKM_PATH)/.ccache/br-arm-full-2019.05.1"
        BR2_TOOLCHAIN_EXTERNAL=y
        BR2_TOOLCHAIN_EXTERNAL_CUSTOM=y
        BR2_TOOLCHAIN_EXTERNAL_DOWNLOAD=y
        BR2_TOOLCHAIN_EXTERNAL_URL="http://autobuild.buildroot.org/toolchains/tarballs/br-arm-full-2019.05.1.tar.bz2"
        BR2_TOOLCHAIN_EXTERNAL_GCC_4_9=y
        BR2_TOOLCHAIN_EXTERNAL_HEADERS_4_14=y
        BR2_TOOLCHAIN_EXTERNAL_LOCALE=y
        # BR2_TOOLCHAIN_EXTERNAL_HAS_THREADS_DEBUG is not set
        BR2_TOOLCHAIN_EXTERNAL_CXX=y
        BR2_TARGET_GENERIC_GETTY_PORT="ttyAMA0"
        BR2_LINUX_KERNEL=y
        BR2_LINUX_KERNEL_CUSTOM_VERSION=y
        BR2_LINUX_KERNEL_CUSTOM_VERSION_VALUE="{}"
        BR2_LINUX_KERNEL_DEFCONFIG="vexpress"
        BR2_LINUX_KERNEL_DTS_SUPPORT=y
        BR2_LINUX_KERNEL_INTREE_DTS_NAME="vexpress-v2p-ca9"
        BR2_TARGET_ROOTFS_SQUASHFS=y
        # BR2_TARGET_ROOTFS_TAR is not set
        BR2_PACKAGE_REALTEK_R8152_LINUX=y
        BR2_PACKAGE_REALTEK_R8152_LINUX_TEST=y
        """.format(KERNEL_VERSION)

    def force_rebuild_of_driver(self):
        self.show_msg("Rebuilding driver under test")
        self.b.build(["realtek-r8152-linux-dirclean", "all"])

    def boot(self):
        img = os.path.join(self.builddir, "images", "rootfs.squashfs")
        self.call_on_host(["truncate", "-s", "%1M", img])

        self.emulator.boot(arch="armv5",
                           kernel=os.path.join(self.builddir, "images", "zImage"),
                           kernel_cmdline=["root=/dev/mmcblk0",
                                           "rootfstype=squashfs"],
                           options=["-drive", "file={},if=sd,format=raw".format(img),
                                    "-M", "vexpress-a9",
                                    "-dtb", os.path.join(self.builddir, "images", "vexpress-v2p-ca9.dtb")])
        self.emulator.login()

    def test_run(self):
        self.force_rebuild_of_driver()
        self.boot()

        self.cleanup_dmesg()

        self.start_kunit_on_module("r8152-test", timeout=120)

        kunitresultfilename = "{}-{}.log".format(self.builddir, "kunitresult")
        self.save_kunit_output(kunitresultfilename)

        dmesgfilename = "{}-{}.log".format(self.builddir, "dmesg")
        self.save_dmesg(dmesgfilename)

        linux_dir = os.path.join(self.builddir, "build", "linux-{}".format(KERNEL_VERSION))
        logfile = infra.open_log_file(self.builddir, "run", self.logtofile)
        output = self.process_kunit_output(kunitresultfilename, linux_dir, logfile)
        self.check_kunit_output(output)
