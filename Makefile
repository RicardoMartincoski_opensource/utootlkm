BR2_EXTERNAL_DIR := $(shell readlink -f .)
BUILDROOT_DIR := $(BR2_EXTERNAL_DIR)/buildroot
TEST_INFRA_DIR := $(BUILDROOT_DIR)/support/testing
DOWNLOAD_CACHE_DIR := $(BR2_EXTERNAL_DIR)/download
CCACHE_DIR := $(BR2_EXTERNAL_DIR)/.ccache
TEST_SRC_DIR := support/testing/tests
TEST_OUTPUT_DIR := $(BR2_EXTERNAL_DIR)/test-output
TEST_TMP_SUBDIR := tests/utootlkm
TEST_TMP_DIR := $(TEST_INFRA_DIR)/$(TEST_TMP_SUBDIR)
TEST_PARAM ?= $(subst /,.,$(TEST_TMP_SUBDIR))
# expose expected output and inner test results to the shell calling 'make V=1 test'
# NOTE: when running multiple outer tests in parallel you may want to 'make V=0 test'
# NOTE: 'make V=2 test' can be use to debug logfiles to serial
V ?= 1

export BASE_TEST_DIR = $(TEST_TMP_SUBDIR)
export BR2_EXTERNAL_DIR
export V

ifeq ($(V),2)
RUN_TESTS_EXTRA_ARGS += -s
endif

.PHONY: default
default: submodules test

.PHONY: submodules
submodules:
	@git submodule init
	@git submodule update

.PHONY: test-inside-docker
test-inside-docker:
	@rm -rf $(TEST_TMP_DIR)
	@mkdir -p $(TEST_TMP_DIR)
	@$(foreach file, $(wildcard $(TEST_SRC_DIR)/*), \
		ln -fs $(shell realpath --relative-to=$(TEST_TMP_DIR) $(file)) $(TEST_TMP_DIR)/; \
		)
	$(TEST_INFRA_DIR)/run-tests $(RUN_TESTS_EXTRA_ARGS) --keep --output $(TEST_OUTPUT_DIR) --download $(DOWNLOAD_CACHE_DIR) --timeout-multiplier 10 $(TEST_PARAM)
	@rm -rf $(TEST_TMP_DIR)

.PHONY: test
test: submodules
	@./docker-run make V=$(V) TEST_PARAM=$(TEST_PARAM) test-inside-docker

.PHONY: clean-test
clean-test:
	@rm -rf $(TEST_TMP_DIR)
	@rm -rf $(TEST_OUTPUT_DIR)

.PHONY: clean
clean: submodules clean-test

.PHONY: distclean
distclean: clean
	@rm -rf $(DOWNLOAD_CACHE_DIR)
	@rm -rf $(CCACHE_DIR)
	@rm -rf $(BUILDROOT_DIR)

.PHONY: .gitlab-ci.yml
.gitlab-ci.yml:
	@rm -rf $(TEST_TMP_DIR)
	@mkdir -p $(TEST_TMP_DIR)
	@$(foreach file, $(wildcard $(TEST_SRC_DIR)/*), \
		ln -fs $(shell realpath --relative-to=$(TEST_TMP_DIR) $(file)) $(TEST_TMP_DIR)/; \
		)
	@cp -f .gitlab-ci.yml.in .gitlab-ci.yml
	@./docker-run /home/br-user/buildroot/support/testing/run-tests --list | grep "$(TEST_PARAM)" | sed -e 's,^test_run (,,g' -e 's,).*,: {extends: .test},g' | sort >> .gitlab-ci.yml
	@rm -rf $(TEST_TMP_DIR)
